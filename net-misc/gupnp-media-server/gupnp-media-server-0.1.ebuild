# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="1"
GCONF_DEBUG="no"

inherit gnome2

DESCRIPTION="A UPNP media server"
HOMEPAGE="http://gupnp.org/sources/gupnp-media-server/"
SRC_URI="http://gupnp.org/sources/gupnp-media-server/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=">=dev-lang/vala-0.3.4
	>=dev-vala/gupnp-vala-0.2
	>=net-libs/gupnp-av-0.1
	>=gnome-base/gconf-2.22
	>=dev-libs/dbus-glib-0.74"
DEPEND="${RDEPEND}
	>=dev-util/pkgconfig-0.9"

DOCS="AUTHORS ChangeLog NEWS README"

