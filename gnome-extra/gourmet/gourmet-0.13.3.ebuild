# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/gnome-extra/sensors-applet/sensors-applet-1.7.9.ebuild,v 1.1 2006/11/02 21:15:28 allanonjl Exp $

inherit gnome2 eutils python

DESCRIPTION="Recipe Manager for GNOME"
HOMEPAGE="http://grecipe-manager.sourceforge.net/"
SRC_URI="mirror://sourceforge/sensors-applet/${P}-1.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="rtf"

RDEPEND=">=dev-lang/python-2.2
	>=x11-libs/gtk+-2.6
	>=dev-python/pygtk-2.6
	>=dev-python/gnome-python-2.10
	>=dev-python/gnome-python-extras-2.14
	gnome-base/libglade
	dev-db/metakit
	dev-python/imaging
	dev-python/reportlab
	>=dev-python/pysqlite-2
	rtf? ( dev-python/pyrtf )"

DEPEND="${RDEPEND}
	>=dev-util/pkgconfig-0.12
	>=app-text/gnome-doc-utils-0.3.2"

DOCS="AUTHORS ChangeLog NEWS README TODO"

pkg_setup() {
	if ! built_with_use dev-db/metakit python; then
		einfo "gourmet needs python bindings for metakit"
		die "Please remerge dev-db/metakit with the python use flag"
	fi
}

src_compile() {
	python setup.py build || die "failed to build ${P}"
	#python setup.py build_desktop || die "failed to build desktop file"
}

src_install() {
	python setup.py install --root=${D} || die "failed to install ${P}"
	#python setup.py install_desktop --root=${D} || die "failed to install desktop file"
}
