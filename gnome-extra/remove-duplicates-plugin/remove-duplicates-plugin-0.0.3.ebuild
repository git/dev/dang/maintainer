# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit gnome2 eutils autotools

DESCRIPTION="Evolution plugin to remove duplicate email"
HOMEPAGE="http://www.gnome.org/~carlosg/stuff/evolution/"
SRC_URI="http://www.gnome.org/~carlosg/stuff/evolution/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND=">=mail-client/evolution-2.12"
DEPEND="${RDEPEND}
	>=dev-util/intltool-0.29"

src_unpack() {
	gnome2_src_unpack

	epatch "${FILESDIR}"/${P}-evo2.12.patch

	eautoreconf
	intltoolize --force
}
