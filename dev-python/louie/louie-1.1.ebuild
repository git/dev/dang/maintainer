# Copyright 2006-2007 BreakMyGentoo.org
# Distributed under the terms of the GNU General Public License v2

inherit distutils

MY_P=${P/louie/Louie}

DESCRIPTION="Signal dispatching in Python"
HOMEPAGE="http://www.pylouie.org/index.html"
SRC_URI="http://cheeseshop.python.org/packages/source/L/Louie/${MY_P}.tar.gz"

RESTRICT="nomirror"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="-* ~amd64 ~ppc ~x86"
IUSE="test"

RDEPEND=">=dev-lang/python-2.4"

DEPEND="${RDEPEND}
	test? ( dev-python/nose )"

S=${WORKDIR}/${MY_P}
