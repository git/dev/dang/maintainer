# Copyright 2006-2007 BreakMyGentoo.org
# Distributed under the terms of the GNU General Public License v2

inherit distutils

MY_P=${P/coherence/Coherence}

DESCRIPTION="Signal dispatching in Python"
HOMEPAGE="http://www.pylouie.org/index.html"
SRC_URI="http://coherence.beebits.net/download/${MY_P}.tar.gz"

RESTRICT="nomirror"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="-* ~amd64 ~ppc ~x86"
IUSE="tracker"

RDEPEND=">=dev-lang/python-2.5
	dev-python/configobj
	dev-python/gst-python
	dev-python/dbus-python
	tracker? ( app-misc/tracker )"

DEPEND="${RDEPEND}"

S=${WORKDIR}/${MY_P}
