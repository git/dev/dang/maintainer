# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils distutils

DESCRIPTION="GTK Bittorrent Client"
HOMEPAGE="http://deluge-torrent.org"
SRC_URI="http://deluge-torrent.org/downloads/${PN}_${PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="net-libs/libtorrent
	dev-libs/boost"
DEPEND="${RDEPEND}"

S=${WORKDIR}/${PN}

src_compile() {
	if ! built_with_use dev-libs/boost threads; then
		eerror "python-libtorrent needs dev-libs/boost built with"
		eerror "the threads USE flag"
		die "Please re-emerge dev-libs/boost with the threads USE flag"
	fi
	epatch ${FILESDIR}/${P}-64-bit.patch
	distutils_src_compile
}

