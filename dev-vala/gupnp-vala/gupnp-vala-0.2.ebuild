# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="1"
GCONF_DEBUG="no"

inherit gnome2

DESCRIPTION="Vala bindings for gupnp"
HOMEPAGE="http://gupnp.org/"
SRC_URI="http://gupnp.org/sources/bindings/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=">=dev-lang/vala-0.3.4
	>=net-libs/gssdp-0.6.1
	>=net-libs/gupnp-0.12
	>=net-libs/gupnp-ui-0.1
	>=net-libs/gupnp-av-0.2.1"
DEPEND="${RDEPEND}
	>=dev-util/pkgconfig-0.9"

DOCS="AUTHORS ChangeLog NEWS README"

