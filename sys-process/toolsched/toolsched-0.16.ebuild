# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Scripts to automatically start programs with scheduling policy"
HOMEPAGE="http://ck.kolivas.org/apps/toolsched/"
SRC_URI="http://ck.kolivas.org/apps/toolsched/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="sys-process/schedtool"
DEPEND="${RDEPEND}"

src_install() {
	dobin toolsched.*
}

pkg_postinst() {
	einfo "To use toolsched, make a directory first in your path, and put"
	einfo "symlinks for commands you want run pointing to the toolshced script"
	einfo "for the policy you want applied to the command.  For example, to"
	einfo "always run emerge at SCHED_IDLEPRIO, make a symlink like this:"
	einfo "ln -s emerge /usr/bin/toolsched.d"
	einfo "The copy of emerge found next in your path will be run."
	einfo "Toolsched scripts are named for scheduling policies from the"
	einfo "schedtool command, so toolshed.i for SCHED_ISO, toolsched.b for"
	einfo "SCHED_BATCH, and toolsched.d for SCHED_IDLEPRIO"
}
