# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sys-kernel/gentoo-sources/gentoo-sources-2.6.24-r4.ebuild,v 1.3 2008/03/31 11:55:02 armin76 Exp $

ETYPE="sources"
K_WANT_GENPATCHES="base extras"
K_GENPATCHES_VER="11"
inherit kernel-2
detect_version
detect_arch

CK_VERSION="2.6.22-ck1"
CK_PATCH="patch-${CK_VERSION}.bz2"
CK_URI="http://www.kernel.org/pub/linux/kernel/people/ck/patches/2.6/2.6.22/2.6.22-ck1/${CK_PATCH}"


UNIPATCH_LIST="${DISTDIR}/${CK_PATCH}"
UNIPATCH_STRICTORDER="yes"

KEYWORDS="amd64 x86"
HOMEPAGE="http://dev.gentoo.org/~dang/ck/"

DESCRIPTION="-ck on top of gentoo-sources"
SRC_URI="${KERNEL_URI} ${GENPATCHES_URI} ${ARCH_URI} ${CK_URI}"

K_EXTRAELOG="If there's issues with this, do not post bugs; instead, contact
dang@gentoo.org directly."

pkg_postinst() {
	kernel-2_pkg_postinst
	einfo "For more info on this patchset, and how to report problems, see:"
	einfo "${HOMEPAGE}"
}
